FROM python:3.9.10-slim-bullseye
      
RUN apt-get update && apt-get install -y \
      curl \ 
      nodejs && \
      rm -rf /var/cache/apt/lists

RUN groupadd -g 2000 -r user && useradd --no-log-init -r -m -s /bin/bash -g user -u 2000 user

USER user

ENV PATH "$PATH:/home/user/.local/bin"

RUN pip install cwltool==3.1.20220210171524 udocker==1.3.1 && pip cache purge

RUN udocker install
